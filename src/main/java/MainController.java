import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.awt.event.ActionEvent;

public class MainController {
    @FXML
    private Label myLabel;

    @FXML
    private TextField myTextBox;

    @FXML
    private void handleButtonClick(javafx.event.ActionEvent event){
        myLabel.setText(myTextBox.getText());
    }
}
